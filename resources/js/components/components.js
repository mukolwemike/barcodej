import Vue from 'vue';

Vue.component('home-slider', require('../components/home/HomeSlider.vue').default);

Vue.component('audio-player', require('../components/audio/AudioPlayer.vue').default);

Vue.component('youtube-player', require('../components/video/YoutubePlayer.vue').default);
