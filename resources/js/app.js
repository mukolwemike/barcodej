require('./bootstrap');

import Vue from 'vue';

import VueRouter from 'vue-router';

Vue.use(VueRouter);

import VueAxios from 'vue-axios';

import axios from 'axios';

Vue.use(VueAxios, axios);

require('./components/components');

import VueCompositionApi from '@vue/composition-api';

Vue.use(VueCompositionApi);

const router = new VueRouter({ mode: 'history'});

import store from './store';

const app = new Vue({
    el: '#app',
    router,
    store
});