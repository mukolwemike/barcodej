import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const state = {
    videos: [],
    activeVideo: {
        videoId: '',
        title: ''
    }
};

const getters = {
    getVideos: state => state.videos,
    getActiveVideo: state => state.activeVideo,
};

const mutations = {
    CHOOSE_VIDEO(state, video) {
        state.activeVideo.title = video.title;
        state.activeVideo.videoId = video.videoId;
    }
};

const actions = {
    getVideos(context) {
        axios.get(
            'https://www.googleapis.com/youtube/v3/playlistItems',
            {
                params: {
                    part: 'snippet',
                    key: 'AIzaSyDmEQIiHCsqziVIeGMV1NRzvF1hAY4wwW8',
                    maxResults: 4,
                    playlistId: 'UUY6CEooRYTsPh-57NviF_BQ'
                }
            })
            .then(({data}) => {
                // set active video
                state.activeVideo.title = data.items[0].snippet.title;
                state.activeVideo.videoId = "https://www.youtube.com/embed/" + data.items[0].snippet.resourceId.videoId;

                // set list videos
                data.items.forEach(function (item, index) {
                    state.videos[index] = {
                        id: index,
                        title: item.snippet.title,
                        creator: item.snippet.channelTitle,
                        thumbnail: item.snippet.thumbnails.medium.url,
                        videoId: "https://www.youtube.com/embed/" + item.snippet.resourceId.videoId
                    }
                });
            });
    }
};

const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions
});

export default store;