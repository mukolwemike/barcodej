<div class="social-links">
    <ul>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
    </ul>
</div>

<div class="copy-text">
    <p>&copy;2019 by BarcodeJ. Proudly created with mukolweSofts.com</p>
</div>