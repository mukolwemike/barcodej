<div class="contact-title">
    <h2>
        <span>
            CONTACT
        </span>
    </h2>
</div>

<div class="contact-form">
    <div class="form-group nameLabel">
        <input type="text" class="form-control" id="nameLabel" name="name" autocomplete="off" required>
        <label for="nameLabel" class="label-name">
            <span class="content-name">Name</span>
        </label>
    </div>
</div>

<br>

<div class="contact-form">
    <div class="form-group emailLabel">
        <input type="email" class="form-control" id="emailLabel" name="email" autocomplete="off" required>
        <label for="emailLabel" class="label-name">
            <span class="content-name">Email</span>
        </label>
    </div>
</div>

<br>

<div class="contact-form">
    <div class="form-group subjectLabel">
        <input type="text" class="form-control" id="emailLabel" name="email" autocomplete="off" required>
        <label for="emailLabel" class="label-name">
            <span class="content-name">Subject</span>
        </label>
    </div>
</div>

<br>

<div class="contact-form">
    <div class="form-group messageLabel">
        <textarea class="form-control" id="messageLabel" rows="2" autocomplete="off" required></textarea>
        <label for="messageLabel" class="label-name">
            <span class="content-message-name">Message</span>
        </label>
    </div>
</div>

<br>

<div class="contact-form">
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
    </div>
</div>
