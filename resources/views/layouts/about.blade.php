<div class="about-title">
    <h2>
        <span>
            ABOUT
        </span>
    </h2>
</div>

<div class="about-header">
        <span>
            Music is the Journey
        </span>
</div>

<div class="about-tagline">
        <span>BarcodeJ is a passionate and committed music professional,
            with the talent and drive to succeed. Growing up amongst the vibrant and diverse
            music scene in Nakuru Town(Nax Vegas), where there is an environment full of inspiration and
            stimulation for BarcodeJnr to feed off. Explore their music portfolio
            and feel free to get in touch with any comments or questions.
        </span>
</div>

<div class="about-backdrop">
    <div class="about-tag">
            <p>
                “Music expresses that which cannot be said and on which it is impossible to be silent”
            </p>

            <p>
                Victor Hugo
            </p>
    </div>
</div>