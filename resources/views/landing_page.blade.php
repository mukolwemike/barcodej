<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Barcode Jnr</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito|Raleway&display=swap" rel="stylesheet">

    <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">

    <div data-spy="scroll" data-target="#barcode-jnr-navbar" data-offset="0">
        <section class="home-section" id="home-section">
            @include('layouts/home')
        </section>

        <section class="slider-section">
            <home-slider></home-slider>
        </section>

        <section class="audio-section" id="audio-section">
            <div class="audio-content">
                <audio-player></audio-player>
            </div>
        </section>

        <section class="about-section" id="about-section">
            @include('layouts/about')
        </section>

        <section class="video-section" id="video-section">
            <youtube-player></youtube-player>
        </section>

        <div class="contact-section" id="contact-section">
            @include('layouts/contact')
        </div>

        <div class="footer-section">
            @include('layouts/footer')
        </div>
    </div>
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
